#Homework 3: Error detection and correction: dealing with homophone confusion

##Overview

This repository contains four parts `sentence`, `feature`, `model` , `src`.

* `sentence` directory is supposed to contain text data in which target words exists

* `feature` directory is supposed to contain feature files generated from the text data.

* `model` directory is supposed to contain SVM model and Language Model Files.

* `src` contains all the source code files
	* `preprocess.py` generates the training data, training features
	* `homophone_clf_train.py` trains SVM model from the feature files
	* `homophone_clf_classify.py` correct the test file using SVM model
	* `homophone_lm_train.py` generates feature file compatible with SRILM
	* `homophone_lm_classify.py` correct the test file using SRILM language model
	

### Usage

* **preprocess.py** :  `./preprocess`
* **homophone_clf_train.py** : `./homophone_clf_train.py`
* **homophone_clf_classify.py** : `./homophone_clf_classify.py [-h] TESTFILE OUTPUTFILE`  
* **homophone_lm_train.py** : `./homophone_lm_train.py`
* **homophone_lm_classify.py** : `./homophone_lm_classify.py [-h] TESTFILE OUTPUTFILE`  


##Approch I : Ngram Language Model

In this approach, homophone pairs are evaluated by 4-gram language model generated by SRILM

###Result Evaluation:

| Label |      Precision     |       Recall       |       F-score      |
|:-----:|:------------------:|:------------------:|:------------------:|
|  it's | 0.8740740740740741 | 0.963265306122449 | 0.916504854368932 |
|  its  | 0.993103448275862 | 0.9744360902255639 | 0.9836812144212522 |
|  lose | 0.8260869565217391 | 0.8837209302325582 | 0.853932584269663 |
| loose | 0.8717948717948718 | 0.8095238095238095 | 0.8395061728395062 |
|  their| 0.9966035904900534 | 0.9889263360616274 | 0.9927501208313195 |
|  they're  | 0.6714285714285714 | 0.8703703703703703 | 0.7580645161290323 |
|  to  | 0.9914725660267031 | 0.9990670725719336 | 0.9952553316376442|
|  too  |0.9616935483870968 | 0.7315950920245399 | 0.8310104529616725|
|  your  |0.9856770833333334 | 0.9755154639175257 | 0.9805699481865285|
|  you're  | 0.8671328671328671 | 0.9185185185185185 | 0.8920863309352518|
| overall | 0.9691554143500133 | 0.9895917815625844  | 0.9792669876939539|

##Approach II : SVM Classification (Selected)

###Feature Selection:

The feature contains context words, context pos tags, suffix of previous word and next word.

###Result Evaluation:

The test result on dev set is listed below:

| Label |      Precision     |       Recall       |       F-score      |
|:-----:|:------------------:|:------------------:|:------------------:|
|  it's |  0.8947368421052632 | 0.9692982456140351 | 0.9305263157894736 |
|  its  | 0.9946605644546148 | 0.9804511278195489 | 0.9875047330556607 |
|  lose | 0.9285714285714286 | 0.9069767441860465 | 0.9176470588235294|
| loose | 0.9069767441860465 | 0.9285714285714286 | 0.9176470588235294 |
|  their| 0.9990277102576568 | 0.9894077997111218 | 0.9941944847605225 |
|  they're  | 0.7027027027027027 | 0.9629629629629629 | 0.8125 |
|  to  | 0.9923869015665414 | 0.9984778552489443 | 0.9954230609197935|
|  too  |0.9411764705882353 | 0.7607361963190185 | 0.8413910093299406|
|  your  |0.9922279792746114 | 0.9871134020618557 | 0.989664082687338|
|  you're  | 0.9280575539568345 | 0.9555555555555556 | 0.9416058394160584|
| overall | 0.9744510978043912 | 0.9898621248986212 | 0.9820961577147456|


## Training Data and External Tools

[Wikicorpus](http://www.cs.upc.edu/~nlp/wikicorpus/) : Training data

[NLTK](https://github.com/nltk/nltk) : Text data processing

[scikit-learn](https://github.com/scikit-learn/scikit-learn) : SVM classification

[SRILM](http://www.speech.sri.com/projects/srilm/) : Language Model

[pysrilm](https://github.com/njsmith/pysrilm) : Python wrapper for SRILM