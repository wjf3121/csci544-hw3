#!/usr/bin/env python3

def calc_precision_recall(predicts, corrects, label):
    retrieved_labels, relevant_labels, correct_labels = 0, 0, 0
    for predict, correct in zip(predicts, corrects):
        if predict == label:
            retrieved_labels += 1
        if correct == label:
            relevant_labels += 1
        if correct == label and predict == label:
            correct_labels += 1
    return (correct_labels / retrieved_labels) if retrieved_labels != 0 else 0, (correct_labels / relevant_labels) if relevant_labels != 0 else 0

def calc_f_score(predicts, corrects, label):
	precision, recall = calc_precision_recall(predicts, corrects, label)
	return precision, recall, (2 * precision * recall / (precision + recall)) if (precision + recall) != 0 else 0
