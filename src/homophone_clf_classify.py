#!/usr/bin/env python3
from utility import SVM_MODEL_PATHS, clf_feature, get_correct, calc_accuracy, correct_sentences, f_score_overall, write_sentences
import pickle
#from homophone_lm_utility import clf_feature_with_suf_lm, read_lm_model
import argparse

def read_svm_calssifier():
	clfs = {}
	for pair in SVM_MODEL_PATHS:
		with open( SVM_MODEL_PATHS[pair], "rb" ) as f:
			clfs[pair] = pickle.load(f)
	return clfs

def svm_classify(feature, feature_porcessor, candidates, classifier):
	clf, vec = classifier[candidates]
	feature = vec.transform(feature_porcessor(feature, candidates))
	return clf.predict(feature)[0]

def svm_classify_with_lm(lm):
	def f(feature, feature_porcessor, candidates, classifier):
		clf, vec = classifier[candidates]
		feature = vec.transform(feature_porcessor(feature, candidates, lm))
		return clf.predict(feature)[0]
	return f



def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('TESTFILE',
						type=str,
						help='Name of the test file')
	parser.add_argument('OUTPUTFILE',
						type=str,
						help='Name of the output file')
	parser.add_argument('-t',
                        type=str,
                        help='Name of the correct dev file')
	args = parser.parse_args()
	svm_classifier = read_svm_calssifier()

	#lm = read_lm_model(str.encode(LM_MODEL_PATH))

	corrected_sens, corrected_tokens = correct_sentences(args.TESTFILE, svm_classify, 
		svm_classifier, clf_feature)
	if args.t != None:
		correct = get_correct(args.t)
		error = get_correct(args.TESTFILE)
		print(calc_accuracy(corrected_tokens, correct))
		f_score_overall(corrected_tokens, correct, error)

	write_sentences(args.OUTPUTFILE, corrected_sens)

if __name__ == '__main__':
	main()