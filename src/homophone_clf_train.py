#!/usr/bin/env python3
import pickle
from sklearn.linear_model import Perceptron
from sklearn import svm
from sklearn.feature_extraction import DictVectorizer
from sklearn import metrics, cross_validation
from collections import defaultdict
from utility import clf_feature, PAIR_SET, FEATURE_PATHS, SVM_MODEL_PATHS, CANDIDATES, \
	clf_feature_with_suf, LM_PROB_PATHS, LM_PROB_PATHS, LM_MODEL_PATH
#from homophone_lm_utility import clf_feature_with_suf_lm, read_lm_model

def get_features(input_path, labels, f_processor = lambda x, y : x, t_processor = lambda x, labels : x, lm = None):
	features, targets = [], []
	with open(input_path, 'r',  encoding='latin-1', errors='ignore') as input_file:
		for line in input_file:
			feature = line.split()
			features.append(f_processor(feature[1:], CANDIDATES[feature[0]], lm))
			targets.append(t_processor(feature[0], labels))
	return features, targets

def train(features, targets):
	vec = DictVectorizer()
	features = vec.fit_transform(features)
	#clf = Perceptron(n_iter=30)
	clf = svm.LinearSVC()
	clf.fit(features, targets)
	return clf, vec

def evaluate(features, targets):
	vec = DictVectorizer()
	features = vec.fit_transform(features)
	#clf = Perceptron(n_iter=30)
	clf = svm.LinearSVC()
	score = cross_validation.cross_val_score(clf, features, targets, scoring='f1', cv=5)
	print(score)
	print(sum(score) / len(score))

def write_models(output_path, clf_vec):
	with open(output_path, 'wb') as handle:
  		pickle.dump(clf_vec, handle)

#target_process = lambda x, labels :1 if x == labels[0] else 0

def main():
	#lm = read_lm_model(str.encode(LM_MODEL_PATH))
	for pair in PAIR_SET:
		print(pair)
		features, targets = get_features(FEATURE_PATHS[pair], pair, clf_feature)
		clf, vec = train(features, targets)
		write_models(SVM_MODEL_PATHS[pair], (clf, vec))
		
if __name__ == '__main__':
	main()
