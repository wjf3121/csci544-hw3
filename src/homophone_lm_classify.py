#!/usr/bin/env python3
from utility import lm_feature, get_correct, calc_accuracy, correct_sentences, LM_MODEL_PATH, write_sentences, f_score_overall
from homophone_lm_utility import lm_prob, read_lm_model, lm_prob2, lm_prob3, lm_prob4
import argparse



def lm_classify(feature, feature_porcessor, candidates, classifier):
	l_f = feature_porcessor(feature)
	p1 = lm_prob(l_f[:3] + [candidates[0]] + l_f[3:], classifier)
	p2 = lm_prob(l_f[:3] + [candidates[1]] + l_f[3:], classifier)
	if p1 != p2:
		return candidates[0] if p1 > p2 else candidates[1]
	p1 = lm_prob2(l_f[:3] + [candidates[0]] + l_f[3:], classifier)
	p2 = lm_prob2(l_f[:3] + [candidates[1]] + l_f[3:], classifier) 
	if p1 != p2:
		return candidates[0] if p1 > p2 else candidates[1]
	p1 = lm_prob3(l_f[:3] + [candidates[0]] + l_f[3:], classifier)
	p2 = lm_prob3(l_f[:3] + [candidates[1]] + l_f[3:], classifier) 
	if p1 != p2:
		return candidates[0] if p1 > p2 else candidates[1]
	p1 = lm_prob4(l_f[:3] + [candidates[0]] + l_f[3:], classifier)
	p2 = lm_prob4(l_f[:3] + [candidates[1]] + l_f[3:], classifier) 
	return candidates[0] if p1 > p2 else candidates[1]



def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('TESTFILE',
						type=str,
						help='Name of the test file')
	parser.add_argument('OUTPUTFILE',
						type=str,
						help='Name of the output file')
	parser.add_argument('-t',
                        type=str,
                        help='Name of the correct dev file')
	args = parser.parse_args()

	lm = read_lm_model(str.encode(LM_MODEL_PATH))
	corrected_sens, corrected_tokens = correct_sentences(args.TESTFILE, lm_classify, lm, lm_feature)
	if args.t != None:
		correct = get_correct(args.t)
		error = get_correct(args.TESTFILE)
		print(calc_accuracy(corrected_tokens, correct))
		f_score_overall(corrected_tokens, correct, error)
	write_sentences(args.OUTPUTFILE, corrected_sens)

if __name__ == '__main__':
	main()