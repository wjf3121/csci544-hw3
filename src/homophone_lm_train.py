#!/usr/bin/env python3
from utility import lm_feature, PAIR_SET, FEATURE_PATHS, CANDIDATES, LM_FEATURE_PATH, LM_MODEL_PATH
from subprocess import call

def get_features(input_path, f_processor = lambda x, y : x):
	features, targets = [], []
	with open(input_path, 'r',  encoding='latin-1', errors='ignore') as input_file:
		for line in input_file:
			tokens = line.split()
			feature = f_processor(tokens[1:])
			feature.insert(3, tokens[0])
			features.append(feature)
	return features

def write_features(output_path, features):
	with open(output_path, 'w') as f:
  		for feature in features:
  			f.write(' '.join(feature) + '\n')


def get_all_features():
	features_all = []
	for pair in PAIR_SET:
		print(pair)
		features = get_features(FEATURE_PATHS[pair], lm_feature)
		features_all.extend(features)
	return features_all


def main():
	write_features(LM_FEATURE_PATH, get_all_features())
	call(['ngram-count' '-text', LM_FEATURE_PATH,  '-lm', LM_MODEL_PATH,  '-order', '4'])

if __name__ == '__main__':
	main()