from srilm import LM
from utility import lm_feature, PAIR_SET, LM_PROB_PATHS, FEATURE_PATHS, LM_MODEL_PATH


def read_lm_model(model_path):
	lm = LM(model_path, lower=True)
	return lm

def lm_prob(feature, lm):
	lm_features = [lm.vocab.intern(str.encode(x)) for x in feature[::-1]]
	prob = 0
	
	for i in range(4):
		prob += lm.logprob(lm_features[i],
           lm_features[i + 1: i + 4])
	
	for i in range(1, 4):
		prob += lm.logprob(lm_features[i],
           lm_features[i + 1: i + 3])

	'''
	for i in range(2, 4):
		prob += lm.logprob(lm_features[i],
           lm_features[i + 1: i + 2])
	'''
	return max(-999, prob)

def lm_prob2(feature, lm):
	lm_features = [lm.vocab.intern(str.encode(x)) for x in feature[::-1]]
	prob = 0
	for i in range(1, 4):
		prob += lm.logprob(lm_features[i],
           lm_features[i + 1: i + 3])
	return max(-999, prob)


def lm_prob3(feature, lm):
	lm_features = [lm.vocab.intern(str.encode(x)) for x in feature[::-1]]
	prob = 0

	for i in range(2, 4):
		prob += lm.logprob(lm_features[i],
           lm_features[i + 1: i + 2])
	return max(-999, prob)

def lm_prob4(feature, lm):
	lm_features = [lm.vocab.intern(str.encode(x)) for x in feature[::-1]]
	prob = 0

	
	prob = lm.logprob(lm_features[3],[])
	return max(-999, prob)

def clf_feature_with_suf_lm(feature, *args):
	new_feature = {}
	new_feature['p_t_1'] = feature[5]
	if len(args) < 2: return new_feature
	l_f = lm_feature(feature)
	candidates = args[0]
	homophone_lm = args[1]
	p1 = lm_prob(l_f[:3] + [candidates[0]] + l_f[3:], homophone_lm)
	p2 = lm_prob(l_f[:3] + [candidates[1]] + l_f[3:], homophone_lm)
	new_feature['lm_1_1'] = p1
	new_feature['lm_1_2'] = p2
	return new_feature



def get_lm_prob_for_features(features, candidates, lm):
	results = []
	for f in features:
		l_f = lm_feature(f)
		r = (lm_prob(l_f[:3] + [candidates[0]] + l_f[3:], lm), lm_prob(l_f[:3] + [candidates[1]] + l_f[3:], lm))
		results.append(r)
	return results

def write_lm_prob_for_features(output_path, probs):
	with open(output_path, 'w',  encoding='latin-1', errors='ignore') as o:
		for p in probs:
			o.write(' '.join([str(x) for x in p]) + '\n')

def generate_model_probs():
	lm = read_lm_model(str.encode(LM_MODEL_PATH))
	for pair in PAIR_SET:
		print(pair)
		features = []
		with open(FEATURE_PATHS[pair], 'r',  encoding='latin-1', errors='ignore') as input_file:
			for line in input_file:
				feature = line.split()[1:]
				features.append(feature)
		probs = get_lm_prob_for_features(features, pair, lm)
		write_lm_prob_for_features(LM_PROB_PATHS[pair], probs)

def read_lm_probs(input_path):
	with open(input_path, 'r',  encoding='latin-1', errors='ignore') as input_file:
		for line in input_file:
			prob  = [float(x) for x in line.split()] 


if __name__ == '__main__':
	generate_model_probs()
