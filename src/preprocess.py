#!/usr/bin/env python3
import os
import re
import json
import nltk
import itertools
from nltk.tokenize import sent_tokenize, word_tokenize
from utility import generate_feature, FEATURE_PATHS, SENTENCE_PATHS, WORDS_DICT, PAIR_SET, SUB_DICT, SUB_ORIGIN
import argparse





#WORD_PAIRS = [('it\'s', 'its'), ('you\'re', 'your'), ('they\'re', 'their'), ('loose', 'lose'), ('to', 'too')]

def get_sentences(dir_path, words_dict, sub_dict, output_path):
	with open(output_path, 'w',  encoding='latin-1', errors='ignore') as o:
		for fn in sorted(os.listdir(dir_path)):
			file_path = os.path.join(dir_path, fn)
			with open(file_path, 'r',  encoding='latin-1', errors='ignore') as f:
				for line in f:
					if line.startswith('<') or line.startswith('ENDOFARTICLE') or not line.strip():
						continue
					sent_tokenize_list = sent_tokenize(line.strip())
					for s in sent_tokenize_list:
						if any(x in (re.sub('[^0-9a-zA-Z\']+', ' ', s).lower()).split() for x in words_dict):
							o.write(s + '\n')
						elif any(x in re.sub('[^0-9a-zA-Z\']+', ' ', s).lower() for x in sub_dict):
							o.write(s + '\n')
						'''
						for w in words_dict:
							if w in s:
								words_dict[w].append(s)
						'''



def get_features_from_sentence(sentence, words_dict, subdict , output_file):
	text = word_tokenize(sentence)
	text_tags = nltk.pos_tag(text)
	for i, tt in enumerate(text_tags):
		if tt[0].lower() in words_dict:
			word = tt[0].lower()
			start = end = i
		elif i < (len(text_tags) - 1) and ((tt[0] + text_tags[i + 1][0]).lower() in words_dict or \
				((tt[0] + ' ' + text_tags[i + 1][0]).lower() in words_dict)):
			word = (tt[0] + text_tags[i + 1][0]).lower() if (tt[0] + text_tags[i + 1][0]).lower() in words_dict else \
					(tt[0] + ' ' + text_tags[i + 1][0]).lower()
			start, end = i, i + 1
		else:
			continue
		if word in subdict:
				word = subdict[word]
		feature = generate_feature(text_tags, start, end)
		feature.insert(0, word)
		output_file.write(' '.join(feature) + '\n')
		#words_dict[word].append(feature)


def get_features(input_path, words_dict, subdict, output_path):
	with open(output_path, 'w',  encoding='latin-1', errors='ignore') as o:
		with open(input_path, 'r',  encoding='latin-1', errors='ignore') as f:
			for i,line in enumerate(f):
				if i % 1000 == 0:
					print(i)
				get_features_from_sentence(line, words_dict, subdict, o)



def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('PATH',
						type=str,
						help='Name of the training data directory')
	args = parser.parse_args()
	for pair in PAIR_SET:
		print(pair)
		get_sentences(args.PATH, set(pair), SUB_DICT[pair], SENTENCE_PATHS[pair])

	for pair in PAIR_SET:
		words_dict = set(pair).union(SUB_DICT[pair])
		print(words_dict)
		get_features(SENTENCE_PATHS[pair], words_dict, SUB_ORIGIN, FEATURE_PATHS[pair])

if __name__ == '__main__':
	main()