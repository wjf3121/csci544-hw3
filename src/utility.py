from nltk.tokenize import word_tokenize
from collections import defaultdict
from f_score import calc_f_score
from word_shape import word_shape
import nltk


WORDS_DICT = set([
				'it\'s', 'its', 
				'you\'re', 'your', 
				'they\'re', 'their', 
				'loose', 'lose', 
				'too', 'to'
			])

PAIR_SET = [
			('loose', 'lose'),
			('they\'re', 'their'),
			('it\'s', 'its',),
			('you\'re', 'your'),
			('too', 'to')
		]

SUB_DICT = {
				('it\'s', 'its',) : set(['it is']),
				('you\'re', 'your') : set(['you are']),
				('they\'re', 'their') : set(['they are']),
				('loose', 'lose') : set([]),
				('too', 'to') : set([])
				
				}
SUB_ORIGIN = {
				'it is' : 'it\'s',
				'you are' : 'you\'re',
				'they are' : 'they\'re'
			}

CANDIDATES = {
				'it\'s' :('it\'s', 'its',), 'its' : ('it\'s', 'its'),
				'you\'re': ('you\'re', 'your'), 'your' : ('you\'re', 'your'),
				'they\'re' : ('they\'re', 'their'), 'their' : ('they\'re', 'their'),
				'loose' : ('loose', 'lose'), 'lose': ('loose', 'lose'),
				'too' : ('too', 'to'), 'to' : ('too', 'to')
			}

FEATURE_PATHS = {
				('it\'s', 'its',) : '../feature/features_its',
				('you\'re', 'your') : '../feature/features_your',
				('they\'re', 'their') : '../feature/features_their',
				('loose', 'lose') : '../feature/features_lose',
				('too', 'to') : '../feature/features_to'
				
				}

SENTENCE_PATHS = {
				('it\'s', 'its',) : '../sentence/sentences_its',
				('you\'re', 'your') : '../sentence/sentences_your',
				('they\'re', 'their') : '../sentence/sentences_their',
				('loose', 'lose') : '../sentence/sentences_lose',
				('too', 'to') : '../sentence/sentences_to'
				
				}

SVM_MODEL_PATHS = {
				('it\'s', 'its',) : '../model/model_its',
				('you\'re', 'your') : '../model/model_your',
				('they\'re', 'their') : '../model/model_their',
				('loose', 'lose') : '../model/model_lose',
				('too', 'to') : '../model/model_to'
			}

LM_PROB_PATHS = {
				('it\'s', 'its',) : '../feature/lm_prob_its',
				('you\'re', 'your') : '../feature/lm_prob_your',
				('they\'re', 'their') : '../feature/lm_prob_their',
				('loose', 'lose') : '../feature/lm_prob_lose',
				('too', 'to') : '../feature/lm_prob_to'
			}


LM_MODEL_PATH = '../model/homophone.lm'

LM_FEATURE_PATH = '../feature/features_lm'

def generate_feature(text_tags, start, end):
	feature = []
	for j in range(start - 3, start):
		token = text_tags[j][0] if j > -1 else 'NONE'
		tag = text_tags[j][1] if j > -1 else 'NONE'
		feature.append(token)
		feature.append(tag)
	for j in range(end + 1, end + 4):
		token = text_tags[j][0] if j < len(text_tags) else 'NONE'
		tag = text_tags[j][1] if j < len(text_tags) else 'NONE'
		feature.append(token)
		feature.append(tag)
	return feature

def clf_feature(feature, *args):
	new_feature = {}
	pgw, ngw = '', ''
	pgt, ngt = '', ''
	
	for i in range(0, 6, 2):
		
		token, tag = feature[i], feature[i + 1]
		new_feature['p_w_' + str(i / 2)] = token.lower()
		#new_feature['p_t_' + str(i / 2)] = tag

		pgw += '_' + token.lower()
		pgt += '_' + tag

		token, tag = feature[i + 6], feature[i + 7]
		new_feature['n_w_' + str(i / 2)] = token.lower()
		#new_feature['n_t_' + str(i / 2)] = tag

		ngw += '_' + token.lower()
		ngt += '_' + tag
	
	new_feature['p_t_1'] = feature[5]
	#new_feature['n_t_1'] = feature[7]
	new_feature['n_suf_1'] = feature[6][-3:].lower()
	
	'''
	new_feature['p_3_g_w'] = pgw
	new_feature['n_3_g_w'] = ngw
	
	new_feature['p_w_t'] = feature[4].lower() + '_' + feature[5]
	new_feature['n_w_t'] = feature[6].lower() + '_' + feature[7]
	new_feature['p_2_g_t'] = feature[5] + '_' + feature[3]
	new_feature['n_2_g_t'] = feature[7] + '_' + feature[9]
	'''
	new_feature['p_2_g_w'] = feature[4].lower() + '_' + feature[2].lower()
	new_feature['n_2_g_w'] = feature[6].lower() + '_' + feature[8].lower()
	return new_feature
	
def clf_feature_with_suf(feature, *args):
	new_feature = clf_feature(feature)
	new_feature['p_suf_1'] = feature[4][-3:].lower()
	new_feature['n_suf_1'] = feature[6][-3:].lower()
	return new_feature


def lm_feature(feature, *args):
	new_feature = [feature[0].lower(), feature[2].lower(), feature[4].lower(),
			 feature[6].lower(), feature[8].lower(), feature[10].lower()]
	return new_feature


def correct_sentence(sentence, classify, classifier, feature_porcessor):
	text = word_tokenize(sentence)
	text_tags = nltk.pos_tag(text)
	token_idx = 0; sen_idx = 0
	corrected_sen = ''
	corrected_token = []
	while sen_idx < len(sentence):
		while sen_idx < len(sentence) and sentence[sen_idx] == ' ':
			corrected_sen += ' '
			sen_idx += 1
		if sen_idx == len(sentence): break

		if text_tags[token_idx][0].lower() in WORDS_DICT:

			word = text_tags[token_idx][0].lower()
			feature = generate_feature(text_tags, token_idx, token_idx)
			correct = classify(feature, feature_porcessor, CANDIDATES[word], classifier)
			corrected_token.append(correct)
			if text_tags[token_idx][0][0].isupper():
				correct = correct[0].upper() + correct[1:]

		elif token_idx < (len(text_tags) - 1) and text_tags[token_idx + 1][0].startswith('\'') and\
			(text_tags[token_idx][0] + text_tags[token_idx + 1][0]).lower() in WORDS_DICT and\
			sentence[sen_idx + len(text_tags[token_idx][0])] != ' ':
			word = (text_tags[token_idx][0] + text_tags[token_idx + 1][0]).lower()
			feature = generate_feature(text_tags, token_idx, token_idx + 1)
			correct = classify(feature, feature_porcessor, CANDIDATES[word], classifier)
			corrected_token.append(correct)
			if (text_tags[token_idx][0] + text_tags[token_idx + 1][0])[0].isupper():
				correct = correct[0].upper() + correct[1:]
			sen_idx += len(text_tags[token_idx][0])
			'''
			while sentence[sen_idx] == ' ':
				sen_idx += 1
			'''
			token_idx += 1
		else:
			correct = text_tags[token_idx][0]
			if text_tags[token_idx][0] == '\'\'' or text_tags[token_idx][0] == '``':
				if sentence[sen_idx] == '\"':
					correct = '\"'
		corrected_sen += correct
		length = len(text_tags[token_idx][0])
		if text_tags[token_idx][0] == '\'\'' or text_tags[token_idx][0] == '``':
			if sentence[sen_idx] == '\"':
				length = 1
		sen_idx += length
		token_idx += 1
	return corrected_sen, corrected_token

def correct_sentences(input_path, classify, classifier, feature_porcessor):
	corrected_sens, corrected_tokens = [], []
	with open(input_path, 'r', encoding='latin-1', errors='ignore') as input_file:
		for i, line in enumerate(input_file):
			sen, token = correct_sentence(line.strip(), classify, classifier, feature_porcessor)
			corrected_tokens.extend(token)
			corrected_sens.append(sen)
			if i % 1000 == 0:
				print(i)
	return corrected_sens, corrected_tokens


def get_correct(file_path):
	count = defaultdict(int)
	corrects = []
	with open(file_path, 'r',  encoding='latin-1', errors='ignore') as f:
		for line in f:
			text = word_tokenize(line)
			token_idx = 0; sen_idx = 0
			for i, t in enumerate(text):
				while sen_idx < len(line) and line[sen_idx] == ' ':
					sen_idx += 1
				if t.lower() in WORDS_DICT:
					corrects.append(t.lower())
					count[t.lower()] += 1
				elif i < (len(text) - 1) and (t + text[i + 1]).lower() in WORDS_DICT and \
					line[sen_idx + len(t)] != ' ':
					corrects.append((t + text[i + 1]).lower())
					count[(t + text[i + 1]).lower()] += 1
				if t == '\'\'' or t == '``':
					if line[sen_idx] == '\"':
						sen_idx += 1
				else:
					sen_idx += len(t)
	return corrects

def calc_f_scores(result, correct):
	for label in WORDS_DICT:
		print(label + ' : ' + str(calc_f_score(result, correct, label)))

def calc_accuracy(result, correct):
	token_count, correct_token_count = defaultdict(int), defaultdict(int)
	count = 0
	for r, c in zip(result, correct):
		token_count[CANDIDATES[c]] += 1
		if r == c:
			count += 1
			correct_token_count[CANDIDATES[c]] += 1
	calc_f_scores(result, correct)
	for canditate in token_count:
		print(str(canditate) + ' ' + str(correct_token_count[canditate] / token_count[canditate]))
	return count / len(correct)


def f_score_overall(result, correct, error):
	correct_count = 0; change_count = 0; correct_change_count = 0
	for r, c, e in zip(result, correct, error):
		if c != e:
			correct_count += 1
			if c == r:
				correct_change_count += 1
		if r != e:
			change_count += 1
	p = correct_change_count / change_count
	r = correct_change_count / correct_count
	f = 2 * p * r / (p + r)
	print (p, r, f)


def write_sentences(output_path, sentences):
	with open(output_path, 'w',  encoding='latin-1', errors='ignore') as f: 
		for s in sentences:
			f.write(s + '\n')


